#!/bin/bash

BASE_DIR=$(readlink -f ./../..)
LINUX_SRC_DIR=${BASE_DIR}/linux-3.18.16-rpi
LINUX_BUILD_DIR=${BASE_DIR}/linux_build
SCRIPTS_DIR=$HOME/Documents/xvisor_project/scripts/rpi_gen_1
OUTPUT_DIR="$BASE_DIR/deliverables"
RPI1_RT_HOST="$OUTPUT_DIR/rpi1-rt-host"

WITH_RT_PATCH="${WITH_RT_PATCH:-yes}"

cd $LINUX_BUILD_DIR
rm -rf *

cd $BASE_DIR
if [ -e "linux-3.18.16-rpi" ]; then
    echo "Linux kernel 3.18.16-rpi previously downloaded"
else
    echo "Retrieving Kernel Code"
    mkdir -p $LINUX_SRC_DIR
    git clone https://github.com/raspberrypi/linux $LINUX_SRC_DIR
    cd $LINUX_SRC_DIR
    git branch kernel_3.18 remotes/origin/rpi-3.18.y --track
    git checkout kernel_3.18
fi

cd $LINUX_SRC_DIR
if test "$WITH_RT_PATCH" = "yes"; then
    echo "Applying RT Patch"
    PATCH=${BASE_DIR}/patches/patch-3.18.16-rt13.patch.gz
    zcat ${PATCH} | patch -p1 -N

fi

echo "Configuring kernel"
cd $LINUX_SRC_DIR
KERNEL=kernel
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabi-
mkdir -p ${LINUX_BUILD_DIR}

if test "$WITH_RT_PATCH" = "yes"; then
    cp ${BASE_DIR}/configs/linux-3.18.16-rpi1-host-rt.config ${LINUX_BUILD_DIR}/.config
    make O=$LINUX_BUILD_DIR oldconfig
fi

echo "Building Kernel"
make O=$LINUX_BUILD_DIR zImage modules dtbs

echo "Moving files to sdcard directory"
mkdir -p ${BASE_DIR}/sdcard
./scripts/mkknlimg arch/arm/boot/zImage ${BASE_DIR}/sdcard/kernel.img
cp arch/arm/boot/dts/*.dtb ${BASE_DIR}/sdcard/
cp arch/arm/boot/dts/overlays/*.dtb* ${BASE_DIR}/sdcard/overlays/

echo "Moving files to deliverables directory"
mkdir -p $OUTPUT_DIR
mkdir -p $RPI1_RT_HOST
mkdir -p "$RPI1_RT_HOST/overlays"
./scripts/mkknlimg arch/arm/boot/zImage $RPI1_RT_HOST/kernel.img
cp arch/arm/boot/dts/*.dtb $RPI1_RT_HOST/
cp arch/arm/boot/dts/overlays/*.dtb* "$RPI1_RT_HOST/overlays/"
