#!/bin/bash

DATE_STRING=`date +"%m-%d-%y-%H-%M"`

OUTPUT_DIR='deliverables'
BENCHMARK_DIR="$OUTPUT_DIR/benchmarks"
RPI1_RT_GUESTS="$OUTPUT_DIR/rpi1-rt-guests"

mkdir -p logs

# Build UBoot bootloader
echo "Building U-Boot bootloader"
./build_uboot.sh | tee "logs/"$DATE_STRING"_build_uboot.log" 2>&1

# Build Root filesystem for linux guest
echo "Building root file system"
./build_rootfs.sh | tee "logs/"$DATE_STRING"_build_rootfs.log" 2>&1

# Build Xvisor and Linux guest
echo "Building Xvisor and linux guest"
./build_xvisor_linux_rt.sh | tee "logs/"$DATE_STRING"_build_xvisor_linux_rt.log" 2>&1

# Build Benchmarks
echo "Building Benchmarks"
cd ../../applications
./compile_benchmarks_rpi.sh | tee  "../scripts/rpi_gen_1/logs/"$DATE_STRING"_benchmarks.log" 2>&1
cd ../scripts/rpi_gen_1

# Load Benchmarks
echo "Loading Benchmarks"
./load_rootfs_benchmarks.sh| tee  "logs/"$DATE_STRING"_load_benchmarks.log" 2>&1

# Copy Deliverables to directory
echo "Placing RT Deliverables"
cd ../../
mkdir -p $OUTPUT_DIR
mkdir -p $BENCHMARK_DIR
mkdir -p $RPI1_RT_GUESTS
cp -f u-boot/u-boot.bin $RPI1_RT_GUESTS/
# cp -f scripts/rpi_gen_1/config.txt $RPI1_RT_GUESTS/
cp -f scripts/rpi_gen_1/boot.scr $RPI1_RT_GUESTS/
cp -f xvisor/build/uvmm.bin $RPI1_RT_GUESTS/
cp -f xvisor/build/arch/arm/board/generic/dts/bcm2835/two_guest_ebmp.dtb $RPI1_RT_GUESTS/
cp -f xvisor/build/disk.img $RPI1_RT_GUESTS/

cp -f applications/run-benchmarks.sh $BENCHMARK_DIR
cp -f applications/dry2 $BENCHMARK_DIR
cp -f applications/dry2nr $BENCHMARK_DIR
cp -f applications/dry2o $BENCHMARK_DIR
cp -f applications/stream $BENCHMARK_DIR
cp -f applications/CachebenchAndHackbench/hackbench $BENCHMARK_DIR
cp -f applications/CachebenchAndHackbench/llcbench/cachebench/cachebench $BENCHMARK_DIR
cp -f applications/CyclicTest/rt-tests/cyclictest $BENCHMARK_DIR

echo "RT Deliverables in Place"

