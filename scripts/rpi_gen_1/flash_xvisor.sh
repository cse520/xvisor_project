#!/bin/bash

BOOT_LOC=$1
FS_LOC=$2

cd ..

# Load U-Boot
echo "Loading U-Boot"
cp -f ../u-boot/u-boot.bin $BOOT_LOC/
cp -f rpi_gen_1/config.txt $BOOT_LOC/
cp -f rpi_gen_1/boot.scr $BOOT_LOC/

# Load Xvisor
echo "Loading Xvisor"
cp -f ../xvisor/build/uvmm.bin $BOOT_LOC/
cp -f ../xvisor/build/arch/arm/board/generic/dts/bcm2835/two_guest_ebmp.dtb $BOOT_LOC
cp -f ../xvisor/build/disk.img $BOOT_LOC

echo "Loading Replacement Host Kernel"
# cp -f ../host/linux/arch/arm/boot/zImage $BOOT_LOC
cp -f ../applications/run-benchmarks.sh $FS_LOC/home/pi
cp -f ../applications/dry2 $FS_LOC/home/pi
cp -f ../applications/dry2nr $FS_LOC/home/pi
cp -f ../applications/dry2o $FS_LOC/home/pi
cp -f ../applications/stream $FS_LOC/home/pi
cp -f ../applications/CachebenchAndHackbench/hackbench $FS_LOC/home/pi
cp -f ../applications/CachebenchAndHackbench/llcbench/cachebench/cachebench $FS_LOC/home/pi
cp -f ../applications/CyclicTest/rt-tests/cyclictest $FS_LOC/home/pi
