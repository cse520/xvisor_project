#!/bin/bash
set -e # stop on error

if [ $EUID != 0 ]; then
    echo "This script should be run with sudo."
    exit
fi

################################################################################
# User config options
BASE_DIR=$(dirname $(readlink -f $0))/../..
IMAGE_DIR=$BASE_DIR/deliverables/rpi2-guests
CONFIGS_DIR=$BASE_DIR/configs
PATCH_DIR=$BASE_DIR/patches
RT_PATCH=${PATCH_DIR}/patch-4.1.13-rt15.patch.gz
TOOL_DIR=$BASE_DIR/toolchains
WITH_GUEST="${WITH_GUEST:-yes}"
WITH_RT="${WITH_RT:-no}"
NUM_CORES=2
USER=rob

BUSYBOX_VER=1.21.1
UBOOT_VER=2015.10
XVISOR_VER=next
LINUX_MAJOR=4
LINUX_MINOR=1
LINUX_PATCH=13
BOARD=realview-pb-a8

BARE_CC=${BASE_DIR}/toolchains/linaro/bin/arm-linux-gnueabihf-
LINUX_CC=${BARE_CC}
BUSYBOX_CC=${BARE_CC}
APP_CC=${BARE_CC}
################################################################################

LINUX_VER=${LINUX_MAJOR}.${LINUX_MINOR}
LINUX_VER_FULL=${LINUX_VER}.${LINUX_PATCH}
XVISOR_SRC_DIR=$BASE_DIR/xvisor-$XVISOR_VER
UBOOT_SRC_DIR=$BASE_DIR/u-boot-$UBOOT_VER
BUSYBOX_SRC_DIR=$BASE_DIR/busybox-$BUSYBOX_VER
LINUX_SRC_DIR=$BASE_DIR/linux-$LINUX_VER_FULL
LINUX_BUILD_DIR=$BASE_DIR/linux_build-rpi2
if test "$WITH_RT" = "yes"; then
    LINUX_BUILD_DIR=${LINUX_BUILD_DIR}-rt
    IMAGE_DIR=${IMAGE_DIR}-rt
fi

XVISOR_DISK_DIR=$XVISOR_SRC_DIR/build/disk/images/arm32/$BOARD
export TARGET=rpi2

su $USER -p <<EOF

export CROSS_COMPILE=${BARE_CC}
mkdir -p $IMAGE_DIR

echo "Making U-boot image"
cd $UBOOT_SRC_DIR
#make mrproper
make rpi_2_defconfig
make -j$NUM_CORES all
cat $XVISOR_SRC_DIR/docs/arm/bcm2836-raspi2-bootblk.bin.ift u-boot.bin > u-boot.hyp

echo "Making config.txt file to boot U-boot"
echo "kernel=u-boot.hyp" > config.txt
echo "kernel_old=1" >> config.txt
echo "gpu_mem=128" >> config.txt

echo "Configuring Xvisor"
cd $XVISOR_SRC_DIR
make ARCH=arm generic-v7-ve-defconfig

echo "Building Xvisor"
make -j$NUM_CORES; make -j$NUM_CORES dtbs
mkimage -A arm -O linux -T kernel -C none -a 0x00008000 -e 0x00008000 -n Xvisor -d build/vmm.bin build/uvmm.bin
make -j$NUM_CORES -C tests/arm32/${BOARD}/basic

echo "Building Xvisor image"
mkdir -p ./build/disk/tmp
mkdir -p ./build/disk/system
cp -f ./docs/banner/roman.txt ./build/disk/system/banner.txt

mkdir -p $XVISOR_DISK_DIR
./build/tools/dtc/dtc -I dts -O dtb -o $XVISOR_DISK_DIR/../$BOARD.dtb ./tests/arm32/$BOARD/$BOARD.dts
cp -f ./build/tests/arm32/$BOARD/basic/firmware.bin $XVISOR_DISK_DIR/firmware.bin
cp -f ./tests/arm32/$BOARD/linux/nor_flash.list $XVISOR_DISK_DIR/nor_flash.list
cp -f ./tests/arm32/${BOARD}/linux/cmdlist ./build/disk/images/arm32/${BOARD}/cmdlist

EOF

# With Linux guest
if test "$WITH_GUEST" = "yes"; then

    su $USER -p <<EOF
    echo "Building Linux kernel $LINUX_VER";
    export CROSS_COMPILE=${LINUX_CC}
    mkdir -p $LINUX_BUILD_DIR

    cd $XVISOR_SRC_DIR
    # cp ./tests/arm32/$BOARD/linux/linux-${LINUX_VER}_realview_pba8_defconfig $LINUX_BUILD_DIR/.config
    cp ${CONFIGS_DIR}/linux-4.1.13-rpi2-guest.config $LINUX_BUILD_DIR/.config
    cd $LINUX_SRC_DIR

    if test "$WITH_RT" = "yes"; then
        echo "Applying RT Patch"
        zcat ${RT_PATCH} | patch -p1 -N
        cp ${BASE_DIR}/configs/linux-4.1.13-rpi2-guest-rt.config ${LINUX_BUILD_DIR}/.config
    fi

    make O=$LINUX_BUILD_DIR ARCH=arm oldconfig
    make -j$NUM_CORES O=$LINUX_BUILD_DIR ARCH=arm Image

    # Reverse the patch so we don't run into problems later
    if test "$WITH_RT" = "yes"; then
        echo "Reversing RT Patch"
        zcat ${RT_PATCH} | patch -p1 -R
    fi
EOF
    
    echo "Building Busybox rootfs";
    export CROSS_COMPILE=${BUSYBOX_CC}
    cd $BUSYBOX_SRC_DIR
    make clean
    su $USER -p <<EOF
    cp $XVISOR_SRC_DIR/tests/arm32/common/busybox/busybox-${BUSYBOX_VER}_defconfig ./.config
    make oldconfig
    make -j$NUM_CORES install
EOF

    cd $BUSYBOX_SRC_DIR
    sudo chown -R root:root ./_install
    fakeroot /bin/bash -c "genext2fs -b 7168 -N 1024 -D $XVISOR_SRC_DIR/tests/arm32/common/busybox/busybox_dev.txt -d ./_install $BUSYBOX_SRC_DIR/rootfs.ext2"

    # Linux rootfs stuff
    mkdir -p $BUSYBOX_SRC_DIR/tmp
    if [ "$(ls -A ./tmp)" ]; then umount ./tmp; fi

    # @todo is loopback really necessary? Why not just put in _install
    # before using fakeroot-genext2fs?
    sudo mount -o loop $BUSYBOX_SRC_DIR/rootfs.ext2 $BUSYBOX_SRC_DIR/tmp
    cd $BUSYBOX_SRC_DIR/tmp
    sudo mkdir -p proc sys dev etc etc/init.d
    sudo cp -f ${BASE_DIR}/scripts/rcS ./etc/init.d/
    sudo chmod +x ./etc/init.d/rcS

    # Install applications

    export CROSS_COMPILE=${APP_CC}

    export APP_BIN="${BASE_DIR}/applications/bin"
    CC=${CROSS_COMPILE}gcc
    MYFLAGS=-s
    APPS="dry2 dry2nr dry2o stream.rpi cachebench hackbench cyclictest"
    su $USER -p <<EOF
    echo "Installing applications to rootfs"
    cd ${BASE_DIR}/applications
    mkdir -p ${APP_BIN}

    $CC $MYFLAGS -c dry.c -o dry1.o -static
    $CC $MYFLAGS -DPASS2 dry.c dry1.o -o ${APP_BIN}/dry2 -static
    $CC $MYFLAGS -DPASS2 -DREG dry.c dry1.o -o ${APP_BIN}/dry2nr -static
    $CC $MYFLAGS -DPASS2 -O dry.c dry1.o -o ${APP_BIN}/dry2o -static
    $CC $MYFLAGS -O -DSTREAM_ARRAY_SIZE=3000000 stream.c -static -o ${APP_BIN}/stream.rpi
    cd ./CachebenchAndHackbench/llcbench
    make distclean; make linux-lam
    cd cachebench
    make
    mv ./cachebench ${APP_BIN}/
    cd ../..
    $CC $MYFLAGS -o ${APP_BIN}/hackbench hackbench.c -static -lpthread
EOF
    cd ${BASE_DIR}/applications/CyclicTest/rt-tests
    make clean; make all
    mv cyclictest ${APP_BIN}/
    mkdir -p $BUSYBOX_SRC_DIR/tmp/bench
    su $USER -p <<EOF
    mkdir -p $IMAGE_DIR/apps
EOF
    for f in ${APPS}; do
        cp ${APP_BIN}/$f $BUSYBOX_SRC_DIR/tmp/bench/;
        su $USER -p <<EOF
        cp ${APP_BIN}/$f $IMAGE_DIR/apps/
EOF
    done;
    cp ${BASE_DIR}/applications/run-benchmarks-rpi.sh $BUSYBOX_SRC_DIR/tmp/bench/
    cp ${BASE_DIR}/applications/delay_start.sh $BUSYBOX_SRC_DIR/tmp/bench/
    
    cd $BUSYBOX_SRC_DIR
    # Create RootFS CPIO image
    # cd $BUSYBOX_SRC_DIR/tmp; sudo find ./ | cpio -o -H newc | gzip -9 > $BUSYBOX_SRC_DIR/rootfs.img; cd -
    umount $BUSYBOX_SRC_DIR/tmp

    su $USER -p <<EOF
    echo "Copying kernel and filesystem";
    cd $XVISOR_SRC_DIR
    cp -f $LINUX_BUILD_DIR/arch/arm/boot/Image $XVISOR_DISK_DIR/Image
    # cp -f $BUSYBOX_SRC_DIR/rootfs.img $XVISOR_DISK_DIR/../rootfs.img
    cp -f $BUSYBOX_SRC_DIR/rootfs.ext2 $XVISOR_DISK_DIR/../rootfs.img
EOF
    
fi

su $USER -p <<EOF
cd $XVISOR_SRC_DIR
echo "Generate full disk image"
# Generate full image
genext2fs -B 1024 -b 16384 -d ./build/disk ./build/disk.img

echo "Creating Xvisor ramdisk"
mkimage -A arm -O linux -T ramdisk -a 0x00000000 -n "Xvisor ramdisk" -d build/disk.img build/udisk.img

echo "Installing files to $IMAGE_DIR"
cd $BASE_DIR
cp ${UBOOT_SRC_DIR}/u-boot.hyp $IMAGE_DIR/
cp ${UBOOT_SRC_DIR}/config.txt $IMAGE_DIR/
mkimage -C none -A arm -T script -d ${BASE_DIR}/scripts/boot-1-rpi2.cmd $IMAGE_DIR/boot.scr
mkimage -C none -A arm -T script -d ${BASE_DIR}/scripts/boot-2-rpi2.cmd $IMAGE_DIR/boot2.scr

cd $XVISOR_SRC_DIR
cp -f build/uvmm.bin $IMAGE_DIR/
cp -f build/arch/arm/board/generic/dts/bcm2836/one_guest_pb-a8.dtb $IMAGE_DIR/
cp -f build/arch/arm/board/generic/dts/bcm2836/two_guest_pb-a8.dtb $IMAGE_DIR/
cp -f build/udisk.img $IMAGE_DIR/

EOF
