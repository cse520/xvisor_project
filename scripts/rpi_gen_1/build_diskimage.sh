#!/bin/bash

LINUX_BUILD_DIR="../linux_build"
BUSYBOX_DIR="../busybox-1.21.1"
XVISOR_DIR="../xvisor"

# Navigate to the scripts directory
cd ../

cd $XVISOR_DIR

echo "	Generating final disk.img for linux guest"
mkdir -p ./build/disk/images/arm32/realview-eb-mpcore
./build/tools/dtc/dtc -I dts -O dtb -o ./build/disk/images/arm32/realview-eb-mpcore.dtb ./tests/arm32/realview-eb-mpcore/realview-eb-mpcore.dts
cp -f ./build/tests/arm32/realview-eb-mpcore/basic/firmware.bin.patched ./build/disk/images/arm32/realview-eb-mpcore/firmware.bin
cp -f ./tests/arm32/realview-eb-mpcore/linux/nor_flash.list ./build/disk/images/arm32/realview-eb-mpcore/nor_flash.list
cp -f ./tests/arm32/realview-eb-mpcore/linux/cmdlist ./build/disk/images/arm32/realview-eb-mpcore/cmdlist
cp -f $LINUX_BUILD_DIR/arch/arm/boot/Image ./build/disk/images/arm32/realview-eb-mpcore/Image
cp -f $BUSYBOX_DIR/rootfs.ext2 ./build/disk/images/arm32/rootfs.img
genext2fs -B 1024 -b 13312 -d ./build/disk ./build/disk.img
