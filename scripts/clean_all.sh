#!/bin/bash

echo "Cleaning busybox"
cd ../busybox-1.21.1
sudo rm -rf _install
rm -f rootfs.ext2
rm -f rootfs.ext2.orig
make clean

echo "Cleaning linux source"
cd ../linux-3.18.24
make mrproper

echo "Cleaning linux build area"
cd ../linux_build
make clean

echo "Cleaning u-boot source"
cd ../u-boot
make clean

echo "Cleaning Xvisor source"
cd ../xvisor
make clean

echo "Cleaning Benchmarks"
cd ../applications
rm -f dry2
rm -f dry2nr
rm -f dry2o
rm -f stream.rpi
cd CachebenchAndHackbench
rm -rf hackbench
cd llcbench/cachebench
rm -f cachebench
cd ../../../../applications/CyclicTest/rt-tests
sudo make clean
cd ../../../scripts

