#!/bin/bash

BUILD_BASE=$HOME/Documents/xvisor_project/host
SCRIPTS_DIR=$HOME/Documents/xvisor_project/scripts/rpi_gen_1

echo "Retrieving Kernel Code"
mkdir -p $BUILD_BASE
cd $BUILD_BASE
git clone https://github.com/raspberrypi/linux
cd linux
git branch kernel_3.16 remotes/origin/rpi-3.16.y --track
git checkout kernel_3.16

echo "Building Kernel"
KERNEL=kernel
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcmrpi_defconfig
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules dtbs

cd $SCRIPTS_DIR