#!/bin/bash

DATE_STRING=`date +"%m-%d-%y-%H-%M"`

mkdir -p logs

echo "Compiling Dhrystone"
arm-none-linux-gnueabi-gcc -c  dry.c -o dry1.o -static >> "logs/"$DATE_STRING"_dhrystone.log" 2>&1
arm-none-linux-gnueabi-gcc -DPASS2  dry.c dry1.o  -o dry2  -static >> "logs/"$DATE_STRING"_dhrystone.log" 2>&1

arm-none-linux-gnueabi-gcc -c -DREG dry.c -o dry1.o  -static >> "logs/"$DATE_STRING"_dhrystone.log" 2>&1
arm-none-linux-gnueabi-gcc -DPASS2 -DREG dry.c dry1.o -o dry2nr  -static >> "logs/"$DATE_STRING"_dhrystone.log" 2>&1

arm-none-linux-gnueabi-gcc -c -O dry.c -o dry1.o  -static >> "logs/"$DATE_STRING"_dhrystone.log" 2>&1
arm-none-linux-gnueabi-gcc -DPASS2 -O dry.c dry1.o -o dry2o -static >> "logs/"$DATE_STRING"_dhrystone.log" 2>&1

echo "Compiling Streams"
arm-none-linux-gnueabi-gcc -O -DSTREAM_ARRAY_SIZE=3000000 stream.c -o stream.rpi -static >> "logs/"$DATE_STRING"_streams_rpi.log" 2>&1
arm-none-linux-gnueabi-gcc -O stream.c -o stream -static >> "logs/"$DATE_STRING"_streams.log" 2>&1

echo "Compiling Cachebench"
cd CachebenchAndHackbench/llcbench
make linux-lam >> "../../logs/"$DATE_STRING"_cachebench.log"
cd cachebench
make >> "../../../logs/"$DATE_STRING"_cachebench.log"

echo "Compiling Hackbench"
cd ../..
arm-none-linux-gnueabi-gcc -o hackbench hackbench.c -static -lpthread >> "../logs/"$DATE_STRING"_hackbench.log" 2>&1

echo "Compiling rt-tests"
cd ../CyclicTest/rt-tests
sudo make all >> "../../logs/"$DATE_STRING"_cycletest.log" 2>&1
