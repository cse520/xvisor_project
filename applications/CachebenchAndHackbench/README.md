*Cachebench*
The LLCBench should be a compiled version. But if for some reason it cannot run, we need to compile it again, run the 'Makefile' in the root directory. 
After which, go to the Cachebench folder, run the compiled 'cachebench'. If error occured, and the error is talking about allocating too much memory, please go to the root folder, find the file named: 'user.def', change '29' to '24'. Run again, it should work. 


*Hackbench*
It is said that Xvisor does not have access to c library, therefore hackbench need to be statically compiled. 
In the folder there is a static compiled version of it. But if for some reason it cannot run, please statically compile 'hachbench.c' again. 
Then just run './hackbench'