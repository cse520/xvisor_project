#!/bin/bash

cd ../../busybox-1.21.1

# Backup original rootfs.ext2 if not already done
if [ -e "rootfs.ext2.orig" ]; then
  echo "Backed up rootfs.ext2 exists"
else
   echo "Backing up rootfs.ext2"
   cp -f rootfs.ext2 rootfs.ext2.orig
fi

# Return to scripts directory
cd -

if [ -e $1 ]; then
   echo "Loading $1 into rootfs.ext2"
else
  echo "$1 not found: please provide filename when calling script."
  exit 1
fi

mkdir -p tmp
sudo mount -o loop ../../busybox-1.21.1/rootfs.ext2 tmp
sudo cp -f $1 tmp/
sudo umount tmp

exit 0
