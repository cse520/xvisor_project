#!/bin/bash

# CHANGE THESE VALUES
DEVICE="/dev/sdc"
IMAGE_NAME="../../2015-05-05-raspbian-wheezy.img"

# Configure Devices
DEV_1=$DEVICE"1"
DEV_2=$DEVICE"2"

#unmount partitions
echo "Unmounting Partitions"
umount $DEV_1
umount $DEV_2

#Flash Raspbian Image
echo "Flashing Image, this may take a while ..."
# dd bs=1M if=2015-05-05-raspbian-wheezy.img of=/dev/sdc
dd bs=4M if=$1 of=$DEVICE

#Make card safe to dismount
echo "Prepping card to dismount"
sync

echo "Flashing Complete"
