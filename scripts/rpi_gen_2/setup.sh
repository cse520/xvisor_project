#!/bin/bash
set -e # stop on error

BASE_DIR=$(dirname $(readlink -f $0))/../..
TOOL_DIR=$BASE_DIR/toolchains
BUILD_DIR=$BASE_DIR/build
LINUX_MAJOR=4
LINUX_MINOR=1
LINUX_PATCH=13
LINUX_VER=${LINUX_MAJOR}.${LINUX_MINOR}.${LINUX_PATCH}
XVISOR_VER=next
UBOOT_VER=2015.10
BUSYBOX_VER=1.21.1

mkdir -p $TOOL_DIR

cd $TOOL_DIR

BASE=gcc-linaro-5.1-2015.08-x86_64_arm-linux-gnueabihf
FILENAME=${BASE}.tar.xz
if [[ ! -f ${FILENAME} ]] ; then
    FOLDER=linaro
    URL=https://releases.linaro.org/components/toolchain/binaries/latest-5.1/arm-linux-gnueabihf/${FILENAME}
    wget $URL
    tar -xvJf ${FILENAME}
    mv $BASE $FOLDER
fi

BASE=arm-2014.05-29-arm-none-linux-gnueabi-i686-pc-linux-gnu
FILENAME=${BASE}.tar.bz2
if [[ ! -f ${FILENAME} ]] ; then
    FOLDER=codesourcery
    URL=https://sourcery.mentor.com/GNUToolchain/package12813/public/arm-none-linux-gnueabi/${FILENAME}
    wget $URL
    tar -xvjf ${FILENAME}
    mv arm-2014.05 $FOLDER
fi

cd $PATCH_DIR
if [[ ! -f ${PATCH_FILE} ]] ; then
    wget https://www.kernel.org/pub/linux/kernel/projects/rt/${LINUX_MAJOR}.${LINUX_MINOR}/${PATCH_FILE}
fi

cd $BASE_DIR

if [[ ! -f linux-${LINUX_VER}.tar.xz ]] ; then
    wget https://cdn.kernel.org/pub/linux/kernel/v${LINUX_MAJOR}.x/linux-${LINUX_VER}.tar.xz
    tar -xvJf linux-${LINUX_VER}.tar.xz
fi

# if [[ ! -f xvisor-${XVISOR_VER}.tar.xz ]] ; then
#     wget http://xhypervisor.org/tarball/xvisor-${XVISOR_VER}.tar.gz
#     tar -xvzf xvisor-${XVISOR_VER}.tar.gz
# fi
if [[ ! -d xvisor-${XVISOR_VER} ]] ; then
    git clone https://github.com/avpatel/xvisor-next
fi

if [[ ! -f u-boot-${UBOOT_VER}.tar.bz2 ]] ; then
    wget ftp://ftp.denx.de/pub/u-boot/u-boot-${UBOOT_VER}.tar.bz2    
    tar -xvjf u-boot-${UBOOT_VER}.tar.bz2
fi

if [[ ! -f u-boot-${BUSYBOX_VER}.tar.bz2 ]] ; then
    wget http://www.busybox.net/downloads/busybox-${BUSYBOX_VER}.tar.bz2    
    tar -xvjf busybox-${BUSYBOX_VER}.tar.bz2
fi
