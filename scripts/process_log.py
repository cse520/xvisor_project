#!/usr/bin/python


def proc_main():

	logName = '../deliverables/runtime_logs/non-rt-host.log'
	cleanedLogName = '../deliverables/runtime_logs/non-rt-host.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()

	###########################################################

	logName = '../deliverables/runtime_logs/rt-host.log'
	cleanedLogName = '../deliverables/runtime_logs/rt-host.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()

	###########################################################

	logName = '../deliverables/runtime_logs/non-rt-single-guest.log'
	cleanedLogName = '../deliverables/runtime_logs/non-rt-single-guest.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()

	###########################################################

	logName = '../deliverables/runtime_logs/non-rt-two-guest.log'
	cleanedLogName = '../deliverables/runtime_logs/non-rt-two-guest.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()

	###########################################################

	logName = '../deliverables/runtime_logs/rt-single-guest.log'
	cleanedLogName = '../deliverables/runtime_logs/rt-single-guest.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()

	###########################################################

	logName = '../deliverables/runtime_logs/rt-double-guest.log'
	cleanedLogName = '../deliverables/runtime_logs/rt-double-guest.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()
	
	###########################################################

	logName = '../deliverables/runtime_logs/non-rt-single-guest-cyclictest.log'
	cleanedLogName = '../deliverables/runtime_logs/non-rt-single-guest-cyclictest.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()
	
	###########################################################

	logName = '../deliverables/runtime_logs/rt-single-guest-cyclictest.log'
	cleanedLogName = '../deliverables/runtime_logs/rt-single-guest-cyclictest.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()
	
	###########################################################

	logName = '../deliverables/runtime_logs/no-rt-two-guest-histogram.log'
	cleanedLogName = '../deliverables/runtime_logs/no-rt-two-guest-histogram.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()
	
	###########################################################

	logName = '../deliverables/runtime_logs/rt-double-guest-histogram.log'
	cleanedLogName = '../deliverables/runtime_logs/rt-double-guest-histogram.clean'

	inFile = open(logName, 'rb')
	outFile = open(cleanedLogName, 'w')

	# Need to clean up the unreadable ascii
	print "Removing Non ASCII text"
	removeNonAsciiBytes( inFile, outFile )

	inFile.close()
	outFile.close()


def removeNonAsciiBytes( inFile, outFile ):

	for line in inFile:
		stripped=''
		for c in line:
			if( (ord(c) >= 32 and ord(c) <= 94) or ( ord(c) >= 97 and ord(c) <= 126 )  or (ord(c) == 10 )):
				stripped = stripped + c
				
		# insert new line if necessary
		index = stripped.find('[guest',1)
		if( index > 0 ):
			before = stripped[index:index+14]
			begin = stripped[0:index]
			stripped = begin + '\n' + before
				
		# export entry to file
		outFile.write( stripped )
		
if __name__ == "__main__":
   proc_main() 

