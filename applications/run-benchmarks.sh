#!/bin/bash

mkdir -p logs
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Kernel Version"
uname -a

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "dry2: 1-50000"
./dry2 ${1-50000} 2>/dev/null | tee -a logs/dry2.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "dry2nr: 1-50000"
./dry2nr ${1-50000} 2>/dev/null | tee -a logs/dry2nr.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "dry2o: 1-50000"
./dry2o ${1-50000} 2>/dev/null  | tee -a logs/dry2o.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
log=streams.log
echo "stream:1"
./stream  | tee -a logs/streams-1.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "stream:2"
./stream  | tee -a logs/streams-2.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "stream:3"
./stream  | tee -a logs/streams-3.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "stream:4"
./stream  | tee -a logs/streams-4.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "stream:5"
./stream  | tee -a logs/streams-5.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "cachebench:p"
./cachebench -p

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "cachebench:b"
./cachebench -b

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "hackbench:1"
./hackbench 1  | tee -a logs/hackbench_1.log
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "hackbench:2"
./hackbench 2  | tee -a logs/hackbench_2.log
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "hackbench:4"
./hackbench 4  | tee -a logs/hackbench_4.log
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "hackbench:6"
./hackbench 6  | tee -a logs/hackbench_6.log
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "hackbench:8"
./hackbench 8  | tee -a logs/hackbench_8.log
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "hackbench:10"
./hackbench 10  | tee -a logs/hackbench_10.log

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "cyclictest:1"
sudo ./cyclictest -l100000 -m -n -a0 -t1 -p95 -i400 -h400 -q | tee -a logs/cyclictest.log
