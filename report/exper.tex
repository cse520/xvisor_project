\section{Experiments}\label{sec:experiments}
The goal of this paper is to
determine the feasibility of running real-time applications within
Xvisor. Xvisor, like all hypervisors, adds an extra layer of
indirection between the application and the physical hardware. The
experiments within this paper focuse on characterizing that hit in
performance that an application will experience.. To generate the characterization, we utilized the
benchmarks discussed previously in order to measure the performance
they see when being ran in a variety of different configurations. For
each of the following configurations, the same script was utilized to
run the benchmarks. 

Due to the recent release of Xvisor allowing Raspberry Pi
2 support, our analysis of the Raspberry Pi 1 results are
more thorough. The Raspberry Pi 2 results should be considered
exploratory, as there are more possible configurations with four
cores. The actual benchmark results used to generate the figures are
available in our supporting documents. Throughout the analysis we
discuss one piece of each figure; with the exception of the cyclictest
histograms, each figure includes the benchmark data for all three
configurations.

\subsection{Host RT versus non-RT}\label{sec:first-conf}
The goal of the first configuration was to test the performance
difference between an RT patched kernel and the unpatched
equivalent. For this configuration, the kernels are running directly
on the Raspberry Pi hardware. This provides an optimal case in which
no overhead is being experienced as a result of Xvisor. It also
provides a comparison point between a kernel with and without the RT
patch being applied.

The experiments revealed that there were no statistically relevant
differences between most benchmarks in this configuration. The only
benchmark to show a significant difference on the Raspberry Pi 1 was
cyclictest. As part of the cyclictest benchmark, we generated a
histogram of the latency that each of the generated tasks
experienced. Figure \ref{fig:cyclic-host-hist} shows the bell curves
for each of the kernels and the curve represents the number of tasks
that was completed at that level of latency. The results indicate that
the RT patch decreases the max (and average) latency associated with
the tasks, verifying that the RT patch has the ability to improve
real-time application performance when running on hardware
directly. To clarify the difference, figure
\ref{fig:cyclic-host-cdf-rpi1} presents a CDF plot of the same data
for the Raspberry Pi 1.

\begin{figure*}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/host-histogram-rpi1}
	  \caption{Pi 1}
	  \label{fig:cyclic-host-hist-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
  	\includegraphics[width=\textwidth]{images/host-histogram-rpi2}
	  \caption{Pi 2}
	  \label{fig:cyclic-host-hist-rpi2}
  \end{subfigure}
  \caption{Cyclictest Host Results}
  \label{fig:cyclic-host-hist}
\end{figure*}

\begin{figure}[h]
	\includegraphics[width=0.48\textwidth]{images/cyclicHostCDF-rpi1}
  \caption{Cyclictest: Host Pi 1 CDF}
  \label{fig:cyclic-host-cdf-rpi1}
\end{figure}

Hackbench \cite{hackbench_man}, a benchmark that uses multiple pairs
of tasks to communicate through either pipes or sockets, showed
differing results on the Raspberry Pi 2. Figure \ref{fig:hackbench}
shows that the time for all the task pairs to complete their
transmissions increases when the RT patch is applied. We believe the
reason for these results comes back to the frequency of context
switching taking place. In the non-rt or partially-preemptivee kernel,
the scheduler will switch the tasks after their time quantum has been
completed. When the RT kernel is running, the tasks will not be
switched until they have no more work to do (since all tasks are
running at the same priority). For hackbench this will not happen
until the tasks fill up a communication buffer, after which they must
yield to allow another thread to run. As a result, the responding task
is unable to receive and process the data which leads to the benchmark
taking longer to complete.

\subsection{Single Guest Performance}\label{sec:second-conf}
The second configuration focuses on the performance hit that comes
about from running the application as a guest within Xvisor. Similar
to the first configuration, the benchmarks are run separately for the
unpatched and RT patched versions of the kernel. This configuration
was chosen as it provides valuable insight as to the amount of
overhead Xvisor places on an application within the guest
machines. It also serves as a valuable indicator of whether utilizing
an RT patched kernel within Xvisor has any benefit for real-time
applications. Most of our graphs present data with both one guest and two
guests, but discussion of the second configuration focuses only on the
comparison between host data and single-guest data.

The experiments for the second configuration showed that an
application incurs performance degradation when running as
a guest on the Raspberry Pi 1. The only benchmark that wasn't affected
by running in a guest within Xvisor was Dhrystone (figure
\ref{fig:dhrystone-rpi1}). The Dhrystone benchmark measures the CPU
performance and the results show that Xvisor provides the guests with
a vast majority of the available CPU cycles. In fact, the Dhrystone
benchmark results weren't affected until multiple guest machines where
executing within Xvisor.

\begin{figure*}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/dhrystone-2-rpi1}
	  \caption{Pi 1}
	  \label{fig:dhrystone-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/dhrystone-2-rpi2}
	  \caption{Pi 2}
	  \label{fig:dhrystone-rpi2}
  \end{subfigure}
    \caption{Dhrystone Results}
    \label{fig:dhrystone}
\end{figure*}

The memory tests, cachebench (figures \ref{fig:cache-memcpy-rpi1} and
\ref{fig:cache-rmw-rpi1}) and stream (figure \ref{fig:stream-rpi1}),
show that memory bandwidth suffered when running on a guest on the
Raspberry Pi 1. Depending on the test, the speed of the memory
transfers would decrease by almost 500 MB/sec during the cachebench
RMW test. This shows that Xvisor comes with enough overhead for memory
accesses that it may not be possible to host data centric applications
within Xvisor and still expect deadlines to be met.

However, the memory results are different on the Raspberry Pi 2, as show in
figures \ref{fig:cache-memcpy-rpi2}, \ref{fig:cache-rmw-rpi2} and
\ref{fig:stream-rpi2}. While we are not entirely sure about the
difference between Raspberry Pi 1 and 2 in this case, one reasonable explanation is the
versions of Xvisor that we used. Several new features
for accessing memory were provided in Xvisor 0.2.7. In addition, the
Raspberry Pi 2 provides hardware virtualization support, as opposed to the
paravirtualization required on the Raspberry Pi 1.

\begin{figure*}
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{images/cyclictest-rpi1}
    \caption{Pi 1}
    \label{fig:cyclictest-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{images/cyclictest-rpi2}
    \caption{Pi 2}
    \label{fig:cyclictest-rpi2}
  \end{subfigure}
  \caption{Cyclictest Summary Statistics}
  \label{fig:cyclictest}
\end{figure*}

The other interesting test was cyclictest for the single guest versus
the host, seen in figure \ref{fig:cyclictest}. The latency of the
kernels increased significantly when being executed within Xvisor. If
you recall, the maximum amount of time it took for either kernel
(Raspberry Pi 1) on the host was 246 microseconds where the maximum
latency seen inside the guest was above 20000 microseconds. The RT
patched and unpatched kernels resulted in similar maximum latencies
for this test (figures \ref{fig:cyclic-guest-hist} and
\ref{fig:cyclic-single-cdf}). This experiment shows that Xvisor is not
able to properly handle real-time applications as guest machines,
whether on Raspberry Pi 1 or 2. This also shows that executing a
kernel with RT patch applied as a guest is no different, from a
latency perspective, than running a baseline kernel. However, if
Xvisor improves its handling of this situation and ceases being the
bottleneck, the RT patch may then play an important role.

\begin{figure*}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/single-histogram-rpi1}
	  \caption{Pi 1}
	  \label{fig:cyclic-guest-hist-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/single-histogram-rpi2}
	  \caption{Pi 2}
	  \label{fig:cyclic-guest-hist-rpi2}
  \end{subfigure}
  \caption{Cyclictest: Single Guest}
  \label{fig:cyclic-guest-hist}
\end{figure*}

\begin{figure}[h]
	\includegraphics[width=0.48\textwidth]{images/cyclicSingleCDF-rpi1}
	\caption{Cyclictest Single CDF}
	\label{fig:cyclic-single-cdf}
\end{figure}

Finally, hackbench also shows a different between host and guest runs
-- on both chips the time for hackbench is increased. However, notice
the time scales for the graph of Raspberry Pi 1 (figure
\ref{fig:hackbench-rpi1}) versus that of Raspberry Pi 2 (figure
\ref{fig:hackbench-rpi2}). The degradation is much more pronounced on
the Raspberry Pi 1 because hackbench uses many threads which must be
scheduled on a single core. So while Xvisor does degrade the
communication of threads, much of the effect on the Raspberry Pi 1
comes from the scheduling bottleneck. We will see more of this effect
in section \ref{sec:third-conf}.

\subsection{One Guest versus Two Guests}\label{sec:third-conf}
The final configuration of this paper focuses on the interference
that guest machines might experience from other guests. In virtualized
environments, the physical hardware is a shared resource that the
hypervisor must monitor and control access. We wanted to determine if
the addition of multiple guests could cause enough interference to
affect the overall performance. To test this, we fire up two guests
within Xvisor at the same time. The clocks within the guests are
synchronized based on the uptime of Xvisor. This provides us with a
facility to launch the benchmarks within the guests within an
acceptable window of time. For this configuration we first discuss the
results for the Raspberry Pi 1 before discussing the Raspberry Pi 2.

On the Raspberry Pi 1, the performance in all of the utilized
benchmarks decreased as a result of having simultaneous machines
executing. Figure \ref{fig:stream-rpi1} shows the bandwidth in MB/sec
that was attainable in all the configurations described
previously. The bandwidth of all the memory operations was cut in
about half as can be seen by the stream results below. The Raspberry
Pi 1 has a single ARM core and thus the two machines are not able to
truly run in parallel, so the results were to be expected. What is not
clear is if this degradation was the sole result of scheduling each
guest or also involved the guests competing for memory. Having a
shared cache likely plays a role, though we discovered late in the
project that the default guest configuration assigns a very small
amount of RAM to each guest. Hence scheduling may be playing a larger
role than memory sharing in this case; further research should utilize
device tree files to assign more memory to each guest and evaluate the
extra interference effect.

\begin{figure*}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/stream-rpi1}
	  \caption{Pi 1}
	  \label{fig:stream-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/stream-rpi2}
	  \caption{Pi 2}
	  \label{fig:stream-rpi2}
  \end{subfigure}
  \caption{Stream Results}
  \label{fig:stream}
\end{figure*}

The Dhrystone benchmark shows that Xvisor handles multiple guest
machines by splitting the hardware resources evenly among all the
guests. This can be altered by adjusting the priority associated with
each of the guest machines. By default, each of our guest machines are
running at the same priority level and are given equal time based on
the round robin scheduler that is utilized by Xvisor. Adjusting the
priority of the guest machines as well as adjusting the Xvisor
scheduler is a possible area of future work.

Interestingly, the effect of having two guests running cyclictest was
not strongly negative, as seen in figures
\ref{fig:cyclic-two-hist-rpi1} and \ref{fig:cyclictest-rpi1}. We
suspect the reason is that cyclictest is not highly utilizing the CPU
(though we have not confirmed this), so that Xvisor can schedule the
two guests without much degradation, even though Xvisor slows down
cyclictest overall. For completeness, a CDF of the cyclictest data with two guests is presented in figure \ref{fig:cyclic-double-cdf}.

Hackbench, however, utilizes more processor cycles. Hence in figure
\ref{fig:hackbench-rpi1} we see the expected result that running two
guests with hackbench decreases performance.

\begin{figure*}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/double-histogram-rpi1}
    \caption{Pi 1}
	  \label{fig:cyclic-two-hist-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/double-histogram-rpi2}
	  \caption{Pi 2}
	  \label{fig:cyclic-two-hist-rpi2}
  \end{subfigure}
  \caption{Cyclictest: Two Guests}
  \label{fig:cyclic-two-hist}
\end{figure*}

\begin{figure}[h]
	\includegraphics[width=0.48\textwidth]{images/cyclicDoubleCDF-rpi1}
	\caption{Cyclictest Double CDF}
	\label{fig:cyclic-double-cdf}
\end{figure}

The results of cachebench followed along with what was witnessed with
the stream benchmark. The performance of the cache once it is ran
within Xvisor and then again when a second guest machine is
running. The drop in the cache performance is a result of having two
guest machines that contain too much content to fit within cache, in
addition to the scheduling bottleneck. As a result, the guests
machines will receive multiple cache miss operations will reduces the
overall performance. In the experiments, cachebench was ran in two
difference configurations based on the type of memory access being
performed. The first mode performs a series of memory copy operations
to measure the performance (figure \ref{fig:cache-memcpy-rpi1}). The second
mode consists of memory being read, modified in some fashion and then
written back into memory (figure \ref{fig:cache-rmw-rpi1}).

\begin{figure*}
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/cachebench-memcpy-rpi1}
	  \caption{Pi 1}
	  \label{fig:cache-memcpy-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/cachebench-memcpy-rpi2}
    \caption{Pi 2}
	  \label{fig:cache-memcpy-rpi2}
  \end{subfigure}
  \caption{Cachebench (memcpy) Results}
  \label{fig:cache-memcpy}
\end{figure*}

\begin{figure*}
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/cachebench-rmw-rpi1}
    \caption{Pi 1}
	  \label{fig:cache-rmw-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/cachebench-rmw-rpi2}
	  \caption{Pi 2}
	  \label{fig:cache-rmw-rpi2}
  \end{subfigure}
  \caption{Cachebench (Read,Modify,Write) Results}
  \label{fig:cache-rmw}
\end{figure*}

The results from the Raspberry Pi 2 are clearer. Dhrystone and stream
exhibit barely noticeable slowdown. This is due to Xvisor being able
to schedule the guests on separate cores and Xvisor small
configuration of guest memory, as mentioned above. The cachebench
results are quite interesting. Figures \ref{fig:cache-memcpy-rpi2} and
\ref{fig:cache-rmw-rpi2} show that for small buffer sizes, two guests
experience little to no degradation, but more degradation at larger
sizes. Our initial intuition was that each core must have its own
private L1 cache, but shared L2 cache. However, further research
revealed that both caches are shared among all cores. We conjecture
that the results have to do with bus traffic on the chip, a low-level
design detail with which we are not familiar. The results for
cyclictest and hackbench are similar to that for the Raspberry Pi 1.

\begin{figure*}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/hackbench-rpi1}
	  \caption{Pi 1}
	  \label{fig:hackbench-rpi1}
  \end{subfigure}
  \begin{subfigure}[b]{0.48\textwidth}
	  \includegraphics[width=\textwidth]{images/hackbench-rpi2}
	  \caption{Pi 2}
	  \label{fig:hackbench-rpi2}
  \end{subfigure}
  \caption{Hackbench Results}
  \label{fig:hackbench}
\end{figure*}

The experiments outlined above were effective in providing us with the
information necessary to generate an assessment. As a reminder, this
research was performed on the Xvisor 0.2.6 release for the first
generation Raspberry Pi and the Xvisor 0.2.7 release for the second
generation. Our research has shown that Xvisor's main problem, in its
default configurations, is that it increases guest OS scheduling
latency. The latency from Xvisor removes any benefits received from
the RT patch when it comes to meeting hard deadlines. Hence Xvisor
needs better facilities or features to support guest OSes scheduling
real-time tasks. Xvisor does allow more advanced configurations,
however, so real-time applications may be suitable on an
expertly-tailored setup.

Most of the other effects involve the scheduling bottleneck on the
single-core Raspberry Pi 1. This is not to say there is no room for
improvement in this area, just that the room to grow is
smaller. Xvisor may, for example, look to research in partitioned
caches to improve memory sharing among guests.
