#!/bin/bash

# See http://elinux.org/RPi_U-Boot

echo "	Compiling Boot Script"
mkimage -A arm -T script -O linux -d two_guest_boot.txt boot.scr

# Navigate to scripts directory(build root directory)
cd ../

export CROSS_COMPILE=$HOME/rpi-tools/arm-bcm2708/arm-bcm2708-linux-gnueabi/bin/arm-bcm2708-linux-gnueabi-
cd ../u-boot

# Cleanup revious build
echo "	Cleaning Build Environment"
make clean

# generate configuration for new build
echo "	Configuring Build"
make rpi_defconfig

# Compile U-boot
echo "	Compiling U-Boot"
make -j8 -s
