#!/bin/bash

# Variable Declaration
LINUX_DIR="../linux-"$LINUX_VERSION
BUSYBOX_DIR="../busybox-1.21.1"
XVISOR_DIR="../xvisor"

export CROSS_COMPILE=arm-none-linux-gnueabi-

# Navigate to scripts root directory (base build directory)
cd ../

# Navigate to xvisor root directory
cd $XVISOR_DIR

# Copy defconfig to Busybox source directory
echo "	Copying guest configuration"
cp tests/arm32/common/busybox/busybox-1.21.1_defconfig $BUSYBOX_DIR/.config

# Move to busybox directory
cd $BUSYBOX_DIR

# Clean out environment
echo "	Cleaning busybox directory"
# make clean
rm -f rootfs.ext2.orig

# Configure Busybox source
echo "	Configuring busybox source"
make oldconfig

# Build Busybox rootfs
echo "	Compiling busybox rootfs"
make install

# Change ownership of installed RootFS
echo "	Changing ownership of directory to allow ext2 file creation"
sudo chown -R root:root ./_install

cd _install
sudo mkdir proc sys dev etc etc/init.d
sudo touch etc/init.d/rcS
sudo cp -f ../../scripts/rcS etc/init.d/
sudo chmod +x etc/init.d/rcS
cd ..

# Create RootFS EXT2 image
echo "	Generating rootfs.ext2 file from compilation files"
fakeroot /bin/bash -c "genext2fs -b 7168 -N 1024 -D ../xvisor/tests/arm32/common/busybox/busybox_dev.txt -d ./_install ./rootfs.ext2"

# Create uncompressed image
# echo "	Generating rootfs.img for initramfs"
# mkdir -p ./tmp
# sudo mount -o loop ./rootfs.ext2 ./tmp
# cd ./tmp; sudo find ./ | cpio -o -H newc | gzip -9 > ../rootfs.img; cd -
# sudo umount ./tmp
