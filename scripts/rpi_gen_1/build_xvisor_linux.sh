#!/bin/bash

# Variable Creation
LINUX_VERSION="3.18.16"
# SOURCE="tests/arm32/realview-eb-mpcore/linux/linux-"$LINUX_VERSION"_realview_ebmp_defconfig"

LINUX_DIR="../linux-"$LINUX_VERSION
LINUX_BUILD_DIR="../linux_build"
BUSYBOX_DIR="../busybox-1.21.1"
XVISOR_DIR="../xvisor"

export CROSS_COMPILE=arm-linux-gnueabi-

# Navigate to scripts directory (build root)
cd ../

cd ../
if [ -e "linux-3.18.16" ]; then
  	echo "Linux kernel previously downloaded"
else
	echo "Retrieving Source Code for Linux Kernel"
  	wget https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.18.16.tar.xz
	tar xvf linux-3.18.16.tar.xz
fi
cd linux-3.18.16

echo "	Cleaning linux directory"
cd $LINUX_DIR
make mrproper

cd $XVISOR_DIR

echo "	Configuring Xvisor build"
make ARCH=arm generic-v6-defconfig

echo "	Building Xvisor"
make; make dtbs

echo "	Packing Xvisor for U-Boot"
mkimage -A arm -O linux -T kernel -C none -a 0x00008000 -e 0x00008000 -n Xvisor -d build/vmm.bin build/uvmm.bin

echo "	Building guest firmware"
make -C tests/arm32/realview-eb-mpcore/basic

# Copy Basic Firmware
echo "	Placing configuration for linux kernel"
mkdir $LINUX_BUILD_DIR
# cp tests/arm32/realview-eb-mpcore/linux/linux-3.16_realview_ebmp_defconfig $LINUX_BUILD_DIR/.config

mkdir -p ../linux_build
cd $LINUX_DIR

# Configure Linux Build Directory
# echo "	Configuring linux build directory"
# make O=$LINUX_BUILD_DIR ARCH=arm oldconfig

cp -f ../configs/linux-3.18.16-rpi1-guest.config $LINUX_BUILD_DIR/.config

make O=$LINUX_BUILD_DIR ARCH=arm oldconfig

# Build Linux in build directory
echo "	Building linux kernel"
make O=$LINUX_BUILD_DIR ARCH=arm Image

# Patch kernel to replace sensitive non-privileged instructions
echo "	Patching kernel for Xvisor compatibility"
$XVISOR_DIR/arch/arm/cpu/arm32/elf2cpatch.py -f $LINUX_BUILD_DIR/vmlinux | $XVISOR_DIR/build/tools/cpatch/cpatch32 $LINUX_BUILD_DIR/vmlinux 0

# Build linux in build directory to reflect changes in kernel image
echo "	Build patched linux kernel"
make O=$LINUX_BUILD_DIR ARCH=arm Image

cd $XVISOR_DIR

echo "	Generating final disk.img for linux guest"
mkdir -p ./build/disk/images/arm32/realview-eb-mpcore
./build/tools/dtc/dtc -I dts -O dtb -o ./build/disk/images/arm32/realview-eb-mpcore.dtb ./tests/arm32/realview-eb-mpcore/realview-eb-mpcore.dts
cp -f ./build/tests/arm32/realview-eb-mpcore/basic/firmware.bin.patched ./build/disk/images/arm32/realview-eb-mpcore/firmware.bin
cp -f ./tests/arm32/realview-eb-mpcore/linux/nor_flash.list ./build/disk/images/arm32/realview-eb-mpcore/nor_flash.list
cp -f ./tests/arm32/realview-eb-mpcore/linux/cmdlist ./build/disk/images/arm32/realview-eb-mpcore/cmdlist
cp -f $LINUX_BUILD_DIR/arch/arm/boot/Image ./build/disk/images/arm32/realview-eb-mpcore/Image
cp -f $BUSYBOX_DIR/rootfs.ext2 ./build/disk/images/arm32/rootfs.img
genext2fs -B 1024 -b 13312 -d ./build/disk ./build/disk.img


