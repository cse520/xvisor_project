#!/bin/bash

echo "Loading Dhrystone"
sudo ./load_rootfs.sh ../../applications/dry2
sudo ./load_rootfs.sh ../../applications/dry2nr
sudo ./load_rootfs.sh ../../applications/dry2o

echo "Loading Streams"
sudo ./load_rootfs.sh ../../applications/stream.rpi

echo "Loading Cachebench"
sudo ./load_rootfs.sh ../../applications/CachebenchAndHackbench/llcbench/cachebench/cachebench

echo "Loading Hackbench"
sudo ./load_rootfs.sh ../../applications/CachebenchAndHackbench/hackbench

echo "Loading Cyclictest"
sudo ./load_rootfs.sh ../../applications/CyclicTest/rt-tests/cyclictest

echo "Loading Scripts"
sudo ./load_rootfs.sh ../../applications/run-benchmarks-rpi.sh
sudo ./load_rootfs.sh ../../applications/delay_start.sh
sudo ./load_rootfs.sh ../../applications/hello.sh

echo "Rebuilding Disk Image"
./build_diskimage.sh
