#!/bin/bash

# Variable Creation
BASE_DIR=$(readlink -f ./../..)
LINUX_SRC_DIR=${BASE_DIR}/linux-3.18.16
SCRIPTS_DIR=$HOME/Documents/xvisor_project/scripts/rpi_gen_1
LINUX_BUILD_DIR=${BASE_DIR}/linux_build
BUSYBOX_DIR=${BASE_DIR}/busybox-1.21.1
XVISOR_DIR=${BASE_DIR}/xvisor

WITH_RT_PATCH="${WITH_RT_PATCH:-yes}"

export CROSS_COMPILE=arm-linux-gnueabi-

cd ${BASE_DIR}
if [ -e "linux-3.18.16" ]; then
  	echo "Linux kernel previously downloaded"
else
	echo "Retrieving Source Code for Linux Kernel"
  	wget https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.18.16.tar.xz
	tar xvf linux-3.18.16.tar.xz
fi

cd ${LINUX_SRC_DIR}
# Apply RT Patch
if test "$WITH_RT_PATCH" = "yes"; then
    echo "Applying RT PREEMPT Patch"
    PATCH=${BASE_DIR}/patches/patch-3.18.16-rt13.patch.gz
    zcat ${PATCH} | patch -p1
fi

echo "	Cleaning linux directory"
make mrproper

cd $XVISOR_DIR

echo "	Configuring Xvisor build"
make ARCH=arm generic-v6-defconfig

echo "	Building Xvisor"
make; make dtbs

echo "	Packing Xvisor for U-Boot"
mkimage -A arm -O linux -T kernel -C none -a 0x00008000 -e 0x00008000 -n Xvisor -d build/vmm.bin build/uvmm.bin

echo "	Building guest firmware"
make -C tests/arm32/realview-eb-mpcore/basic

# Copy Basic Firmware
echo "	Placing configuration for linux kernel"
mkdir -p $LINUX_BUILD_DIR
# cp tests/arm32/realview-eb-mpcore/linux/linux-3.16_realview_ebmp_defconfig $LINUX_BUILD_DIR/.config
cp ${BASE_DIR}/configs/linux-3.18.16-rpi1-guest-rt.config ${LINUX_BUILD_DIR}/.config

cd $LINUX_SRC_DIR

# Configure Linux Build Directory
echo "	Configuring linux build directory"
make O=$LINUX_BUILD_DIR ARCH=arm oldconfig

# Build Linux in build directory
echo "	Building linux kernel"
make O=$LINUX_BUILD_DIR ARCH=arm Image

# Patch kernel to replace sensitive non-privileged instructions
echo "	Patching kernel for Xvisor compatibility"
$XVISOR_DIR/arch/arm/cpu/arm32/elf2cpatch.py -f $LINUX_BUILD_DIR/vmlinux | $XVISOR_DIR/build/tools/cpatch/cpatch32 $LINUX_BUILD_DIR/vmlinux 0

# Build linux in build directory to reflect changes in kernel image
echo "	Build patched linux kernel"
make O=$LINUX_BUILD_DIR ARCH=arm Image

cd $XVISOR_DIR

echo "	Generating final disk.img for linux guest"
mkdir -p ./build/disk/images/arm32/realview-eb-mpcore
./build/tools/dtc/dtc -I dts -O dtb -o ./build/disk/images/arm32/realview-eb-mpcore.dtb ./tests/arm32/realview-eb-mpcore/realview-eb-mpcore.dts
cp -f ./build/tests/arm32/realview-eb-mpcore/basic/firmware.bin.patched ./build/disk/images/arm32/realview-eb-mpcore/firmware.bin
cp -f ./tests/arm32/realview-eb-mpcore/linux/nor_flash.list ./build/disk/images/arm32/realview-eb-mpcore/nor_flash.list
cp -f ./tests/arm32/realview-eb-mpcore/linux/cmdlist ./build/disk/images/arm32/realview-eb-mpcore/cmdlist
cp -f $LINUX_BUILD_DIR/arch/arm/boot/Image ./build/disk/images/arm32/realview-eb-mpcore/Image
cp -f $BUSYBOX_DIR/rootfs.ext2 ./build/disk/images/arm32/rootfs.img
genext2fs -B 1024 -b 13312 -d ./build/disk ./build/disk.img


