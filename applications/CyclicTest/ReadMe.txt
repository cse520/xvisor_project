Steps:

1. git clone git://git.kernel.org/pub/scm/linux/kernel/git/clrkwllms/rt-tests.git

2. cd rt-tests

3. sudo make all
There might be a problem where it says an error about numa.h, 
it it emerges, open Makefile and change NUMA’s value from 1 to 0
Reference: http://www.spinics.net/lists/linux-rt-users/msg14097.html

4. cp ./cyclictest /usr/bin/

5. When running cyclictest, need to be root or use sudo




Below from: https://rt.wiki.kernel.org/index.php/Cyclictest
=====================================================


Usage:

cyclictest V 0.42
Usage:
cyclictest <options>

-a [NUM] --affinity        run thread #N on processor #N, if possible
                           with NUM pin all threads to the processor NUM
-b USEC  --breaktrace=USEC send break trace command when latency > USEC
-B       --preemptirqs     both preempt and irqsoff tracing (used with -b)
-c CLOCK --clock=CLOCK     select clock
                           0 = CLOCK_MONOTONIC (default)
                           1 = CLOCK_REALTIME
-C       --context         context switch tracing (used with -b)
-d DIST  --distance=DIST   distance of thread intervals in us default=500
-E       --event           event tracing (used with -b)
-f       --ftrace          function trace (when -b is active)
-i INTV  --interval=INTV   base interval of thread in us default=1000
-I       --irqsoff         Irqsoff tracing (used with -b)
-l LOOPS --loops=LOOPS     number of loops: default=0(endless)
-m       --mlockall        lock current and future memory allocations
-n       --nanosleep       use clock_nanosleep
-N       --nsecs           print results in ns instead of ms (default ms)
-o RED   --oscope=RED      oscilloscope mode, reduce verbose output by RED
-O TOPT  --traceopt=TOPT    trace option
-p PRIO  --prio=PRIO       priority of highest prio thread
-P       --preemptoff      Preempt off tracing (used with -b)
-q       --quiet           print only a summary on exit
-r       --relative        use relative timer instead of absolute
-s       --system          use sys_nanosleep and sys_setitimer
-T TRACE --tracer=TRACER   set tracing function
    configured tracers: unavailable (debugfs not mounted)
-t       --threads         one thread per available processor
-t [NUM] --threads=NUM     number of threads:
                           without NUM, threads = max_cpus
                           without -t default = 1
-v       --verbose         output values on stdout for statistics
                           format: n:c:v n=tasknum c=count v=value in us
-D       --duration=t      specify a length for the test run
                           default is in seconds, but 'm', 'h', or 'd' maybe add
ed
                           to modify value to minutes, hours or days
-h       --histogram=US    dump a latency histogram to stdout after the run
                           US is the max time to be be tracked in microseconds
-w       --wakeup          task wakeup tracing (used with -b)
-W       --wakeuprt        rt task wakeup tracing (used with -b)


-b is a debugging option to control the latency tracer in the realtime preemption patch.

It is useful to track down unexpected large latencies on a system. This option does only work with

CONFIG_PREEMPT_RT=y
CONFIG_WAKEUP_TIMING=y
CONFIG_LATENCY_TRACE=y
CONFIG_CRITICAL_PREEMPT_TIMING=y
CONFIG_CRITICAL_IRQSOFF_TIMING=y
kernel configuration options enabled. The USEC parameter to the -b option defines a maximum latency value, which is compared against the actual latencies of the test. Once the measured latency is higher than the given maximum, the kernel tracer and cyclictest is stopped. The trace can be read from /proc/latency_trace

mybox# cat /proc/latency_trace >trace.log

Please be aware that the tracer adds significant overhead to the kernel, so the latencies will be much higher than on a kernel with latency tracing disabled.

-c CLOCK selects the clock, which is used

0 selects CLOCK_MONOTONIC, which is the monotonic increasing system time. This is the default selection
1 selects CLOCK_REALTIME, which is the time of day time.
CLOCK_REALTIME can be set by settimeofday, while CLOCK_MONOTONIC can not be modified by the user.

This option has no influence when the -s option is given.

-d DIST set the distance of thread intervals in microseconds (default is 500us)

When cylictest is called with the -t option and more than one thread is created, then this distance value is added to the interval of the threads.

Interval(thread N) = Interval(thread N-1) + DIST

-i INTV set the base interval of the thread(s) in microseconds (default is 1000us)

This sets the interval of the first thread. See also -d.

-l LOOPS set the number of loops (default = 0(endless))

This option is useful for automated tests with a given number of test cycles. cyclictest is stopped once the number of timer intervals has been reached.

-n use clock_nanosleep instead of posix interval timers

Setting this option runs the tests with clock_nanosleep instead of posix interval timers.

-p PRIO set the priority of the first thread

The given priority is set to the first test thread. Each further thread gets a lower priority:

Priority(Thread N) = Priority(Thread N-1)

-q run the tests quiet and print only a summary on exit

Useful for automated tests, where only the summary output needs to be captured

-r use relative timers instead of absolute

The default behaviour of the tests is to use absolute timers. This option is there for completeness and should not be used for reproducible tests.

-s use sys_nanosleep and sys_setitimer instead of posix timers

Note, that -s can only be used with one thread because itimers are per process and not per thread. -s in combination with -n uses the nanosleep syscall and is not restricted to one thread

-t NUM set the number of test threads (default is 1), -t without an argument makes the number of threads equal to the number of cpus

Create NUM test threads. See -d, -i and -p for further information.

-v output values on stdout for statistics

This option is used to gather statistical information about the latency distribution. The output is sent to stdout. The output format is

n:c:v

where n=task number c=count v=latency value in us

Use this option in combination with -l

The OSADL Realtime LiveCD project provides a script to plot the latency distribution.

Expected Results

tglx's reference machine
All tests have been run on a Pentium III 400MHz based PC.

The tables show comparisons of vanilla Linux 2.6.16, Linux-2.6.16-hrt5 and Linux-2.6.16-rt12. The tests for intervals less than the jiffy resolution have not been run on vanilla Linux 2.6.16. The test thread runs in all cases with SCHED_FIFO and priority 80. All numbers are in microseconds.

Test case: clock_nanosleep(TIME_ABSTIME), Interval 10000 microseconds,. 10000 loops, no load.
Commandline: cyclictest -t1 -p 80 -n -i 10000 -l 10000

Kernel	min	max	avg
2.6.16	24	4043	1989
2.6.16-hrt5	12	94	20
2.6.16-rt12	6	40	10
Test case: clock_nanosleep(TIME_ABSTIME), Interval 10000 micro seconds,. 10000 loops, 100% load.
Commandline: cyclictest -t1 -p 80 -n -i 10000 -l 10000

Kernel	min	max	avg
2.6.16	55	4280	2198
2.6.16-hrt5	11	458	55
2.6.16-rt12	6	67	29
Test case: POSIX interval timer, Interval 10000 micro seconds,. 10000 loops, no load.
Commandline: cyclictest -t1 -p 80 -i 10000 -l 10000

Kernel	min	max	avg
2.6.16	21	4073	2098
2.6.16-hrt5	22	120	35
2.6.16-rt12	20	60	31
Test case: POSIX interval timer, Interval 10000 micro seconds,. 10000 loops, 100% load.
Commandline: cyclictest -t1 -p 80 -i 10000 -l 10000

Kernel	min	max	avg
2.6.16	82	4271	2089
2.6.16-hrt5	31	458	53
2.6.16-rt12	21	70	35
Test case: clock_nanosleep(TIME_ABSTIME), Interval 500 micro seconds,. 100000 loops, no load.
Commandline: cyclictest -t1 -p 80 -i 500 -n -l 100000

Kernel	min	max	avg
2.6.16-hrt5	5	108	24
2.6.16-rt12	5	48	7
Test case: clock_nanosleep(TIME_ABSTIME), Interval 500 micro seconds,. 100000 loops, 100% load.
Commandline: cyclictest -t1 -p 80 -i 500 -n -l 100000

Kernel	min	max	avg
2.6.16-hrt5	9	684	56
2.6.16-rt12	10	60	22
Test case: POSIX interval timer, Interval 500 micro seconds,. 100000 loops, no load.
Commandline: cyclictest -t1 -p 80 -i 500 -l 100000

Kernel	min	max	avg
2.6.16-hrt5	8	119	22
2.6.16-rt12	12	78	16
Test case: POSIX interval timer, Interval 500 micro seconds,. 100000 loops, 100% load.
Commandline: cyclictest -t1 -p 80 -i 500 -l 100000

Kernel	min	max	avg
2.6.16-hrt5	16	489	58
2.6.16-rt12	12	95	29
