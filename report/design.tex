\section{Design}\label{sec:design}
There are four primary areas of the system that evaluated for performance. These areas were specifically chosen as they represent the components of a system that can have a significant impact on its real-time performance. A series of benchmarks that are used across academia and industry for measuring the performance of those components were chosen.

Below is a list of the benchmarks chosen for the research:

\begin{itemize}

\item Dhrystone
\item STREAM
\item Hackbench
\item Cachebench
\item Cyclictest

\end{itemize}

\subsection{Dhrystone}
The first benchmark was chosen in order to provide a test metric of the overall processor performance. The first benchmark is called Dhrystone and measures the integer performance of the processor in the system\cite{weicker1984}. It was developed after looking at the common building blocks, such as procedure calls and pointer indirection, of several programming languages\cite{weicker1984}. A representative mix of these operations was then considered and developed into a benchmark\cite{weicker1984}. The benchmark consists of these operations being performed within a loop. The performance of a processor is measured by the number of loops, or dhrystones, that it is able to perform in a second. The Dhrystone measurement is a combination of results from three executables called dry2, dry2o, and dry2nr. The dry2 executable is built without any optimizations being applied by the developer or the compiler. The dry2o executable utilizes compiler optimizations to determine performance benefit. The dry2nr executable simulates the performance benefit obtained from software developer optimizations, such as register keyword. These three applications are able to provide an overview of the processor performance when utilizing different compilation strategies.

\subsection{STREAM}
The second benchmark was chosen to provide a characterization of the memory bandwidth that is available. In many systems, the buffers utilized by the application are able to fit within the cache of the processor; however, there are applications in which the data is not able to fit within the cache. The applications that are unable to fit into cache result in a significant performance hit on memory bandwidth as a result of the cache misses. The STREAM benchmark is utilized to measure the bandwidth of memory operations in the experiments discussed in a later section\cite{stream}. The STREAM benchmark allocates buffers that are specifically designed to not be able to fit within the cache of the processor\cite{stream}. The memory performance is measured in terms of the number of bytes transmitted in a second.

\subsection{Hackbench}
The third benchmark focuses on the scheduler built into the operating system. The scheduler of the operating system controls which applications are allowed to run on the processor. As such, the scheduler is able to play a big role in the overall performance of an application. The hackbench benchmark is utilized to measure the performance of the corresponding schedulers. The hackbench benchmark measures the length of time it takes multiple pairs of processes, or threads, to communicate\cite{hackbench_man}. The communications are performed through the use of sockets or pipes\cite{hackbench_man}. The amount of time it takes for all the communications to complete will be dependent on the number of pairs and the scheduler built into the operating system\cite{hackbench_man}. The benchmark provides a mechanism to verify that a scheduler is able to handle dependencies between multiple processes or threads.

\subsection{Cachebench}
The fourth benchmark focuses on measuring the performance of the system memory cache. As a reminder, the STR\-EAM benchmark specializes in measuring the memory performance when buffers are too large to fit within the cache on the system\cite{stream}. Cachebench, the fourth benchmark, is specifically designed to measure the performance received from the cache hierarchy on the system\cite{cachebench_tech_report}. As such, it performs a series of memory operations on buffers that are located within the cache on the system\cite{cachebench_tech_report}. Between STR\-EAM and cachebench, the experiments are able to provide insight into the overall memory performance as seen from an application perspective. The benchmarks complement each other in regards to their area of coverage. Cachebench provides multiple modes for testing the bandwidth of the system cache such as read, modify, and write. In this mode, the benchmark will read a buffer from cache, modify it in some way, and then write the updated content back into the cache\cite{cachebench_tech_report}. Similar to the STR\-EAM benchmark, cachebench measure its performance in terms of the number of bytes per second that it is able to move around cache memory.

\subsection{Cyclictest}
The first four benchmarks are not directly related to real-time applications as they are just as applicable to general purpose computing. Another important aspect of this research is to determine how real-time applications are able to operate within Xvisor. The final benchmark is called cyclictest and focuses on measuring the kernel latency\cite{cyclictest_tech_report}. The cyclictest benchmark works by creating threads or processes under different schedulers and measuring the number of deadline misses\cite{cyclictest_tech_report}. The Linux kernel comes with the SCHED\_DEAD\-LINE scheduler which is an implementation of Earliest Deadline First (EDF) scheduling. Cyclictest provides facilities to verify and measure differences in the latencies associated with different schedulers being utilized\cite{cyclictest_tech_report}.

By utilizing the metrics from the above benchmarks, we will be able to characterize the performance of the Xvisor hypervisor and the guests running within.
