Dhrystone
file(s): dry.c
source: http://homepages.cwi.nl/~steven/dry.c
dependencies: none
compile: Compiled and run together, see below
run: sh dry.c (needs to be changed to be compiled separately when we run it on a host)
info: The file itself
Measure machine speed using integer operations
Guest Execution Process
1. Run compile_dhrystone.sh script to perform cross compile
2. Load the dry2 dry2nr and dry2o into rootfs.ext2 by using the load_rootfs.sh script in scripts/rpi_gen1
3. Run build_diskimage.sh script from scripts/rpi_gen_1
4. Flash images on SD card
5. Start linux guest
6. Run "./dry2 ${1-50000} 2>/dev/null > dry2_log.txt" in command line
7. Run "./dry2nr ${1-50000} 2>/dev/null > dry2nr_log.txt"
8. Run "./dry2o ${1-50000} 2>/dev/null > dry2o_log.txt"
9. Cat log contents to see results of run. NOTE: Files are not persistent across power cycles.

Hackbench
file(s): rt-tests/
source: Fetch URL: git://git.kernel.org/pub/scm/linux/kernel/git/clrkwllms/rt-tests.git
dependencies: build-essential, libnuma-dev?
compile: make
run: rt-tests/hackbench [options]
info: https://rt.wiki.kernel.org/index.php/Cyclictest
Rt-tests is a collection of tests, but the one we are about (for now) is hackbench. Just type make and then run hackbench. It requires the essential build tools, and also claims that libnuma-dev is required. However, I could not find that package (using apt-get), but it seemed to compile and run just fine. The rt-tests folder is a git submodule.

Stream
file(s): stream.c
source: https://www.cs.virginia.edu/stream/FTP/Code/stream.c
dependencies: build-essential? Just a compiler, really
compile: gcc -O stream.c -o stream
run: ./stream [options?]
info: https://www.cs.virginia.edu/stream/ref.html
The STREAM benchmark is a simple synthetic benchmark program that measures sustainable memory bandwidth (in MB/s) and the corresponding computation rate for simple vector kernels.
