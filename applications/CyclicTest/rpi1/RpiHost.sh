#!/bin/bash

# This one is compiled for Rapbian Wheezy
sudo ./cyclicT -l100000 -m -n -a0 -t1 -p99 -i400 -h400 -q

# -l: number of loops
# -m: lock current and future memory allocations
# -n: use clock_nanosleep
# -a0: run thread 0 on processor 0
# -t1: 1 thread
# -p99: set priority of highest prio thread
# -i400: base interval of thread in us, without it default is 1000
# -h400: dump a latency histogram to stdout after the run
# -q: quiet, print only summary
