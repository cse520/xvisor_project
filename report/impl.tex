\section{Implementation}\label{sec:implementation}
As mentioned previously, the performance evaluations are being performed on a first and second generation Raspberry Pi. With the small form factor and power requirements, the Raspberry Pi SoC is representative of the growing trend in which computers become smaller and more widespread within the devices. The experiments were performed on the Raspberry Pi and the guests executing within Xvisor.

\subsection{Raspberry PI Boot Process}
The Raspberry Pi consists of multiple components, but the two primary ones are the ARM processor and the Broadcom GPU. The Broadcom GPU handles the process of initializing the Raspberry Pi and loading a kernel to be executed based on the system configuration\cite{rpi}. After a default load of Raspbian, the system is configured to execute the kernel that is provided as part of that distribution. It was necessary to reconfigure the system to execute a software package that we provided. The GPU utilizes a configuration file within the boot partition, to control which kernel will be executed.

We utilized bootloading software called U\-Boot \cite{uboot} which is an open source bootloader commonly used throughout industry. The Raspberry Pi configuration was updated so that it would load U\-Boot when the device was powered on. U\-Boot would then handle the process of loading the Xvisor binary and its configurations. Once loaded into memory, the software would yield control over to Xvisor.

\subsection{Xvisor}
Xvisor a type-1 hypervisor that is able to run on ARM processors\cite{xvisor}. Xvisor provides the concept of a virtual CPU (VCPU) to each of the guests. The VCPU of the guests are then assigned time on the physical CPU (PCPU) based on their priority level and the scheduler utilized within Xvisor\cite{patel2015}. The number of VCPU for each guest is controlled through the use of a device tree script (DTS) that is loaded into memory by U\-Boot. These DTS files make it possible to control whether one or two guests are created by Xvisor at startup. We utilize a DTS that generates two guest machines, even if we aren't necessarily executing both machines.

In order to support guest machines, Xvisor presents an emulated hardware layer to the guests. The guest machines believe that they are running on the actual hardware as opposed to the emulated hardware from Xvisor. The guest operating systems believe that they are running on a Realview-EB-MPCore evaluation board on the first generation Raspberry Pi and on a Realview-PB-A8 on the second generation. Xvisor supports a variety of other boards as well.

We are utilizing Xvisor 0.2.6 to run our guests on first generation hardware and Xvisor 0.2.7 for the second generation. All guest machines on the Raspberry Pi 1 are compiled for and running on an emulated Realview-EB-MPCore board, while Raspberry Pi 2 Xvisor requires an emulated Realview-PB-A8 board. These boards were chosen because support was already built into Xvisor and that allowed us to focus on the performance aspect of our project.

\subsection{Linux Kernel}
For our research on the first generation Pi, all the machines, both
physical and virtual, are running the same version of the Linux
kernel. We chose to use the 3.18 version of the Linux kernel for a few
reasons. First, this version of the kernel was already supported on
the Raspberry Pi. Second, it contains the SCHED\_DEAD\-LINE scheduler
which was desirable for testing. The SCHED\_DEAD\-LINE scheduler
was introduced to the Linux kernel in version 3.14 and is present in
all subsequent versions. Having access to this scheduler provides the
ability to test EDF scheduling within our kernels, although we did not
explicitly test applications requiring EDF due to time
limitations. For the Raspberry Pi 2 we chose Linux kernel 4.1 for
similar reasons.

We have the Linux kernel running on the Raspberry Pi
directly as well as guests within Xvisor. The kernels for our
experiments were built from source to provide us the control. 
The kernels were kept as similar as possible
through the process, but some differences were necessary. For
instance, the kernel for the guest machines need to be patched in
order to correctly run within Xvisor. The Linux kernel contains
instructions that affect Xvisor's ability to effectively control access
to the physical resources. As such, after the kernel is built it gets
ran through a script that replaces those sensitive instructions with
ones that provide the control back to Xvisor. Once patched, that
kernel is only able to run within the Xvisor hypervisor as those
instructions are not available on all platforms.

Xvisor provides a default configuration file for the guest
machines. This configuration file for Raspberry Pi 1 was targeted at
the Linux 3.16 kernel, but provided the foundation that allowed us to
get Linux 3.18 operational. The other important difference between the
builds is the hardware that the kernels were compiled to support. The
kernel for the Raspberry Pi used a configuration file that enabled the
drivers for the hardware physically present on the board. The guest
machines within Xvisor do not have access to all the physical hardware
and are presented with a virtualized hardware interface. The kernels
were compiled with the drivers necessary to support the virtualized
Realview-EB-MPCore or Realview-PB-A8 hardware.

\subsection{Linux with RT Patch}
The Linux kernel does not inherently support real-time application
deadlines and preemption requirements. As a result, the kernel allows
priority inversion to take place in multiple locations which causes
deadlines to be missed. By default, a process in Linux can only be
preempted when it is running code within userspace. The Linux kernel
is unable to be preempted while its running any operations regardless
of priority level. Imagine the situation in which a low priority
process performs an IO operation which interrupts the kernel and it
begins processing the IO request. If a high priority process then needs access
to the same resource it must wait until the kernel has completed the
processing for the low priority process. This is a a cause
of priority inversion within the default Linux kernel. Due to these
issues, the default Linux kernel is typically only useful in
situations where soft real-time deadlines are allowed to be utilized.

An open source project was started with the focus of fixing these
issues and bringing support for hard real-time deadlines. The project
is called the RT PRE\_EMPT patch and it makes the following changes to
the Linux kernel:

\begin{itemize}

\item Spinlocks reimplemented with rtmutexes
\item Critical sections are now pre-emptible
\item Implementation of priority inheritance protocol (PIP)
\item Convert interrupt handlers to kernel threads that are pre-emptible
\item Utilization of the high resolution kernel timers

\end{itemize}

In order to perform our real-time experiments, we needed to apply this
RT PRE\_EMPT patch to all of the 3.18 kernels that are utilized as
part of our experiments. The kernels within our experiments were
compiled to exhibit a fully preemptive kernel as described above. This
would result in the least amount of priority inversion taking place
throughout the kernel. In turn, this will lead to the best performance
for real-time applications who have strict timing deadlines that must
be met.

In addition to adding a fully preemptive kernel, we needed the ability
to switch the scheduler that is being utilized by the kernel to
control which processes are given time on the physical hardware. The
Linux kernel defaults to the completely fair scheduler (CFS) to handle
the process of distributing time on the hardware. The CFS scheduler
provides each of the processes with the same amount of time on the CPU
regardless of the priority levels. As such, this scheduler can also
lead to priority inversion taking place on the system even though the
kernel supports full preemption. We replaced the
CFS scheduler with one that implements the EDF algorithm for
determining which processes to give CPU time. This means that the
process with the earliest deadline will always be given the priority
for accessing the CPU. When this is combined with a fully preemptive
kernel, the priority inversion levels are brought to a low enough
level for us to run our experiments on real-time applications.
