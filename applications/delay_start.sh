#!/bin/ash

holdoff=$1

while [ true ]; do
   min=`date +"%M"`
   sec=`date +"%S"`
   
   echo "Minute:"$min", Seconds: "$sec
   
   if [ "$min" -ge "$holdoff" ]; then
      break
   fi
   
   sleep 10

done

start=`date +"%H:%M:%S"`
echo "Executing Benchmarks-"$start
./run-benchmarks-rpi.sh
end=`date +"%H:%M:%S"`

echo "Benchmarks Completed: Start= $start, End:=$end"

