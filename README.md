Xvisor Hypervisor on Raspberry Pi
=================================

Description
-------------------------------
Xvisor is a type-1 hypervisor that was recently ported to the Raspberry Pi system on a chip. Virtualization technologies, such as Xvisor, are growing in use throughout industry. They are especially becoming prevalent in embedded devices where it is desirable to have application isolation. There are many benefits to utilizing virtualization technologies such as greater CPU utilization and device consolidation. Virtualization comes with it's own set of disadvantages as well such as increased overhead for the guest machines. The hypervisor necessarily adds an extra layer of indirection to a guest running within it.

One area of research that has not been performed is whether Xvisor is able to support hosting real-time applications without leading to degraded performance. Xvisor has been utilized with general purpose operating systems but not with real-time applications that have hard deadlines that must be met. The goal of this research is to determine if Xvisor is able to host a real-time application and still allow it to meet all of it's deadline correctly. For this research, we focused on getting Xvisor operational on both generations of the Raspberry Pi computer. The second generation comes with increased amount of ram and multiple processor cores. 

This research was performed as a group project for CSE520: Real-time Operating Systems at Washington University in St. Louis. Throughout the semester, we captured a series of capability demonstrations in the form of videos posted to Youtube. The videos can be found on at the following channel: https://www.youtube.com/channel/UCnu2WtN0ivorpdeduEM5ZxQ.

Group Members:

1. Tony Dong
2. Robert Utterback
3. Cameron Whipple

Directory Layout
-------------------------------
This section of the README discusses the directory structure that is present within the
repository. The directories at the root of the repository are applications, busybox,
configs, modules, patches, report, scripts, u-boot, xvisor, and deliverables.

The applications directory contains the source code for all the applications and scripts that are ran within the guest virtual machines or on the host. The compile_benchmarks_script.sh script handles the process of compiling all the benchmarks for the virtual machines and the host device. The build scripts within this directory are ran as part of the automated build process that is discussed below.

The busybox directory contains the source code for the Busybox filesystem. The guest machines use busybox as their root filesystem. Busybox is an open source project aimed at providing a filesystem ideal for embedded systems. As a result, all the common applications that are present within a Linux system is built into a single image. This makes is possible to avoid duplication of libraries within multiple executable applications.

The configs directory contains the prebuilt configurations for building all the kernels in the repository. During build, the scripts will pull these configuration files as necessary to the build directories. This then allows them to successfully compile all the kernels. There are separate configurations for the guests and the host.

The modules directory contains source code for kernel modules that allow access to the time stamp counter on Raspberry Pi hardware. Due to time limitations, these have never been tested and are not used. This would have allowed for more fine-grained timing of benchmark results.

The patches directory contains the patches that are used to apply the RT PREEMPT patch to the kernels. These patches are pulled at runtime to the appropriate location to convert the kernel into one that supports preemption.

The report directory contain the text files and build process to generate the final report regarding the research performed. Navigating to this directory and issuing the 'make' command will generate the final report. The report is written in latex and the make command will generate a PDF from the latex.

The scripts directory is the heart of the build process for the repository. The scripts directory contains the script files that handle the build process of the repository. The scripts directory is further broke down into a rpi_gen_1 and rpi_gen_2 directories. The rpi_gen_1 contains the scripts for the first generation Raspberry Pi. Likewise, the rpi_gen_2 directory contains the scripts for the second generation Raspberry Pi. These directories contain scripts for handling all aspects of the system. It contains scripts for setting up the devlopment environment, setup_environment.sh, as well as automating the entire build process, build_all.sh or build_all_rt.sh. The build process automatically places a copy of the generated file in the deliverables directory that is discussed below.

The u-boot directory contains the source code for the u-boot software. U-Boot is an open source bootloader that is commonly used on embedded platforms in industry. This project had a required for u-boot so that we can load xvisor into memory in order to begin it's execution. The build process for u-boot is handled through a script that is housed within the scripts directory.

The xvisor directory contains the source code for the Xvisor hypervisor. Xvisor is a type-1 hypervisor that has recently been ported to ARM processors. A hypervisor allows multiple operating systems to be ran at the same time on a piece of hardware. As such, we use xvisor in our research to determine whether it is able to support real-time applications within the guests. Xvisor is built using scripts that are provided within the scripts directory.

The deliverables directory contains the final build of the binaries that get loaded onto the hardware. The idea is that the repository can be cloned and then the images pulled from the deliverables folder in order to verify they work. This makes it easier to repeat the research without needing to go through the entire build process.

Build Environment
-------------------------------
The code within this repository is compiled 3 different cross
compilation tool-chains for the ARM processor architecture. The
Codesourcery tool-chain is used in the compilation of all the kernels
and benchmarks throughout the build process for the Raspberry
Pi 1. The U-Boot bootloader for Raspberry Pi 1is compiled using the
rpi-tools located at git://git.denx.de/u-boot.git. The linaro
toolchains is used to compile everything for the Raspberry Pi 2.

A setup_environment.sh script is available at the root of the scripts
directory which will automatically download all the tools for the
Raspberry Pi 1, including tool-chains, that are necessary for
compiling the software within this repository. The
scripts/rpi_gen_2/setup.sh scripts provides the same service for the
Raspberry Pi 2. Unfortunately, the Raspberry Pi 2 build process is
still a bit fragile, so do not be surprised if it requires
modifications.

The build processes utilized within this repository were developed on a system running
Ubuntu 14.04 - 32 bit and Ubuntu 15.04 - 64 bit. The build process has not been tested/verified on any other
systems and thus your mileage may vary.

Build Steps
-------------------------------
1. Download May 5, 2015 copy of Raspbian from https://www.raspberrypi.org/downloads. 
    It may be necessary to look at the older, archived, releases to find the correct version.

2. Flash an SD card with the Raspbian image downloaded in step 1. A script can be found in the
    scripts directory to assist with this process, flash_image_sd.sh. It may be necessary to modify this
    script so that it points at the appropriate /dev/sdX device.

    WARNING: We provide this script as is and without any warranty. We are not responsible for any damage
             or loss of data that may arise from the use of this script. You will need to make sure that
             you point the script at the appropriate device prior to calling the script. If not, it becomes
             possible to completely clear the contents of an undesired hard disk or flash device.

3. Run the setup_environment.sh script to install all the tools necessary to perform the compilation.

4. Navigate to the scripts/rpi_gen_1 directory

5. Run the build_all.sh script to begin build process of kernels and guests without the real time patch being applied

6. Run the build_all_rt.sh script to begin the build process of the kernels and guests with the real time patch being applied

7. Run the build_linux_3.18_host.sh script to build the fully pre-emptive kernel for the 
Raspberry Pi host.

8. Navigate to scripts/rpi_gen_2 and run setup.sh to setup the build process for Raspberry Pi 2.

9. Run "sudo ./build-rpi2.sh" to build U-boot, Xvisor, Linux and the root filesystem.

10. Run "sudo WITH_RT=yes ./build-rpi2.sh" to do the same with a real-time Linux kernel.

11. Compile a real-time Linux kernel for use on the Raspberry Pi 2. We have not automated this build process; it is similar to building the real-time Linux kernel for the Raspberry Pi 1. Download from github.com/raspberrypi/linux, configure for Raspberry Pi 2, download and apply the correct RT_PATCH, reconfigure with full preemption, and compile with an acceptable toolchain, such as from github.com/raspberrypi/tools.

12. Navigate to the reports directory at the root of the repository

13. Issue the make command in order to build the report, latex needs to be installed for this step. 'sudo apt-get install texlive-full'

14. Insert SD card to card reader an take note of mount points for boot partition and the root filesystem partition.

15. Navigate to the deliverables directory

16. Copy the benchmarks directory to "/home/pi" on the root filesystem partition of the SD card

17. Copy all the contents from the desired binary directory into the boot partition. For instance, loading the rpi1-no-rt-guests files to the boot partition will result in Xvisor booting and the guests not utilizing the RT patch.

18. Eject/Unmount SD card from system and insert to Raspberry Pi.

19. Power on the Raspberry Pi and connect to it via RS232 serial lines in the GPIO header block.
      If using a USB to TTY cable for the serial, such as this http://www.adafruit.com/products/954, then run the open_rpi_terminal.sh script. This will open a screen session with the Raspberry PI to allow communicating through the serial Tx and Rx pins in the GPIO header.
      NOTE: Follow guide on the linked webpage for setup instructions associated with the TTY to USB cable.

Supported Runtime Configurations
-------------------------------
The build process and deliverables in this repo support multiple configurations. These configurations are as follows:

Raspberry Pi 1:
1. Linux 3.18 kernel without RT patch running on the host Raspberry Pi
2. Linux 3.18 kernel with RT patch running on the host Raspberry Pi
3. Single or Double guests running Linux 3.18 kernels without RT patch running in Xvisor
4. Single or Double guests running Linux 3.18 kernels with RT patch running in Xvisor

Raspberry Pi 2:
1. Linux 4.1 kernel without RT patch running on the host Raspberry Pi
2. Linux 4.1 kernel with RT patch running on the host Raspberry Pi
3. Single or Double guests running Linux 4.1 kernels without RT patch running in Xvisor
4. Single or Double guests running Linux 4.1 kernels with RT patch running in Xvisor

To run the experiments, we needed the ability to easily swap out the configurations on the Raspberry Pi. The Raspberry Pi consists of two partitions on the SD card that is used during the boot process. The first partition is called the boot partition and contains the configuration files and the kernel image that gets executed. The second partition contains the root filesystem that becomes available when a kernel is executed on the host.

Below are the instructions for swapping the boot configurations as desired for the Raspberry Pi 1. Similar instructions apply for the Raspberry Pi 2.

1. Linux 3.18 running on Raspberry Pi without RT patch

* Navigate to "deliverables/rpi1-no-rt-host"
* Copy all files to the boot partition of the SD. Replace if necessary.
* Copy the "deliverables/benchmarks" directory to the /home/pi directory on the root filesystem partition.

2. Linux 3.18 running on Raspberry Pi with RT patch

* Navigate to "deliverables/rpi1-rt-host"
* Copy all files to the boot partition of the SD. Replace if necessary.
* Copy the "deliverables/benchmarks" directory to the /home/pi directory on the root filesystem partition.

3. Linux 3.18 guests without RT patch running within Xvisor

* Navigate to "deliverables/rpi1-no-rt-guests" directory
* Copy all the files to the boot partition.

4. Linux 3.18 guests with RT patch running within Xvisor

* Navigate to "deliverables/rpi1-rt-guests" directory
* Copy all the files to the boot partition.

We have not described how to change the configuration from one guest to two guests. By default the Raspberry Pi 1 deliverables are set to boot a configuration with two guests. To change this you must compile a new boot.scr file and use the proper dtb file. See the boot scripts for more details. For Raspberry Pi 2 you can swap between two pre-made scripts. The boot.scr will boot a configuration with one guest, while boot2.scr boots two guests. Simply rename boot.scr something else and rename boot2.scr to boot.scr, and it will be automatically loaded.

Launching Guests within Xvisor
-------------------------------
When a configuration supporting Xvisor with guests is loaded on the Raspberry Pi, it becomes possible to launch the guest machines and bind to their serial. After power-on, you will be presented with an Xvisor prompt:


```
#!

ooooooo  ooooo oooooo     oooo ooooo  .oooooo..o   .oooooo.   ooooooooo.  
 `8888    d8'   `888.     .8'  `888' d8P'    `Y8  d8P'  `Y8b  `888   `Y88.
   Y888..8P      `888.   .8'    888  Y88bo.      888      888  888   .d88'
    `8888'        `888. .8'     888    `Y8888o.  888      888  888ooo88P' 
   .8PY888.        `888.8'      888        `Y88b 888      888  888`88b.   
  d8'  `888b        `888'       888  oo     .d8P `88b    d88'  888  `88b. 
o888o  o88888o       `8'       o888o 8''88888P'   `Y8bood8P'  o888o  o888o

XVisor# 
```


The xvisor prompt contains several built in commands that can be used to interact with Xvisor. These commands can be accessed through the help command:


```
#!

XVisor# help
help         - displays list of all commands
net          - network commands
rbd          - ram backed block device commands
fb           - frame buffer commands
input        - input device commands
vscreen      - virtual screen commands
vinput       - virtual input device commands
vdisplay     - virtual display commands
vdisk        - virtual disk commands
module       - module related commands
wallclock    - wall-clock commands
heap         - show heap status
stdio        - standard I/O configuration
chardev      - character device commands
thread       - control commands for threads
memory       - memory manipulation commands
guest        - control commands for guest
vcpu         - control commands for vcpu
devtree      - traverse the device tree
host         - host information
shutdown     - shutdown hypervisor
reset        - reset hypervisor
version      - show version of hypervisor
vserial      - virtual serial port commands
blockdev     - block device commands
rtcdev       - rtc device commands
vfs          - vfs related commands
```


For the experiments, the guest and vserial portion of the above commands are utilized. The guest command contains operations that are used for interacting with the guests an contains features like starting and stopping a guest. The vserial command allows for binding, or accessing, the serial lines from the guests through Xvisor's virtualized serial framework.

To list available guests issue the following command. This can be used to verify that the guests were successfully created prior to attempting to start them.

```
#!

XVisor# guest list
-------------------------------------------------------------------------------
 ID     Name              Endianness    Device Path                            
-------------------------------------------------------------------------------
 0      guest0            little        /guests/guest0                         
 1      guest1            little        /guests/guest1                         
-------------------------------------------------------------------------------
```


To launch guest0, the guest command will be used to "kick" the machine into action.

```
#!

Xvisor# guest kick guest0
```


After a guest is running, the next step is to connect to that machines serial output and input. This allows for interacting directly with the guests.

```
#!

Xvisor# vserial bind guest0/uart0
```


After attaching to the serial, you'll see a screen similar to the following:


```
#!

[guest0/uart0] ARM Realview-EB-MPCore Basic Firmware
[guest0/uart0] 
[guest0/uart0] basic# 
```


At this point, you are in control of the virtualized hardware that is seen by the guest systems. The firmware has helpful commands within it as well but have not been included in this guide for the sake of brevity.

The next step is to tell the firmware to begin loading and executing the guest kernel. The project is configured to automatically tell the firmware where to find the kernel image. When ready, issue the "autoexec" command to start kernel.


```
#!

[guest0/uart0] ARM Realview-EB-MPCore Basic Firmware
[guest0/uart0] 
[guest0/uart0] basic# autoexec
```


Once completed, the guest's kernel will be operational and you'll have a serial line to that guest.


```
#!

[guest0/uart0] 
[guest0/uart0] Please press Enter to activate this console. 
[guest0/uart0] / # uname -a
[guest0/uart0] Linux (none) 3.18.16-rt13 #4 PREEMPT RT Mon Nov 30 14:40:54 CST 2015 armv6l GNU/Linux
```

Simultaneously Running Benchmarks in Guests
-------------------------------
Each of the guest machines are loaded with the statically compiled versions of the benchmarks.
Along with those benchmarks is a script that can be used to run all the benchmarks and obtain the
results. For our research, it was desirable to be able to run the benchmarks simultanously across
multiple guests within Xvisor. The ideal situation would have provided us with an ethernet connection
to all the guests and that could be used to initiate the benchmarks. At this time, the guest machines do 
not support ethernet connections.

In a desktop or laptop, the date command in linux provides the current date and time for that system. Within the guests,
the date command provides the amount of time that Xvisor has been running. This value remains the same across all the
guest machines as it is tied to the hypervisor. We utilize this date command through the delay_start.sh script, found in
applications directory, to handle the simultaneous release of the benchmarks.

The following example shows the process of delaying the execution of the benchmarks until Xvisor has been running for three minutes.

```
#!

[guest0/uart0] 
[guest0/uart0] Please press Enter to activate this console. 
[guest0/uart0] / # uname -a
[guest0/uart0] ./delay_start.sh
```

The first generation of Raspberry Pi computers only have a single core. As such, it is not possible for all the guests to begin
executing their benchmarks at the exact same time. This timer facility provides a method for getting the benchmarks to begin running
within the same vicinity of time across the guests.


Links
-------------------------------
Below are links to the different software projects that are utilized in the building
or running of Xvisor and the guest machines.

1. http://www.busybox.net
2. http://elinux.org/ARMCompilers
3. http://elinux.org/RPi_U-Boot
